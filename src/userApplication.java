/*
 * App created by Dimitrios Sourlantzis
 * Computer Networks I Project
 * sourland@ece.auth.gr
 * 
 */

import ithakimodem.*;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;

//Codes from Ithaki

public class userApplication {
    static String echo_request_code = "E8198\r";
    static String image_request_code ="M0014\r";
    static String image_request_codeWithError = "G4130\r";
    static String GPS_request_code = "P6234\r";
    static String ACK_request_code = "Q6916\r";
    static String NACK_request_code = "R0428\r";

    //Main: We call the start function to demonstrate the application's function.
    
    public static void main(String[] args) throws IOException{
        new userApplication().start();
        
    }

//Initialize the modem - Set speed and timeout time and print a Welcome message
    public static Modem moden_init(){
        int k;

        Modem modem;
        modem = new Modem();
        modem.setSpeed(80000);
        modem.setTimeout(2000);
        modem.open("ithaki");

        String message = "";

        for(;;) {
            try{
                k = modem.read();
                if (k == -1) break;
                message +=(char)k;
                if(message.indexOf("\r\n\n\n") > -1) {
                    System.out.println("End of welcome message ");
                    break;
                } 
            }   catch (Exception x) {
                break;
            }
        }
        return modem;
    }
    
    public void start() throws IOException{
        System.out.println("Time of session: " + LocalDateTime.now());
        
        Modem modem = moden_init();
        
        echoPackages(modem);
        ARQ_handling(modem);
        imageErrorFree(modem);
        imageWithError(modem);
        GPS_cords_image(modem);
        
        System.out.println("\nEnding session time: " + LocalDateTime.now());
    }
    
    /*
     * Receive a large number of echo packages and register them into a text file as well as the time between the packages
     * and the average time a package need to be sent.
     */
    public static void echoPackages(Modem modem) throws FileNotFoundException {
        
        int k;
        int packages = 0;
        
        File Output_from_echo = new File("Session1/Echo.txt");
		File Time_for_echo = new File("Session1/Time.txt");

		FileOutputStream echo_stream = new FileOutputStream(Output_from_echo);
		FileOutputStream time_stream = new FileOutputStream(Time_for_echo);
        
		long begin = System.currentTimeMillis();
        
		
        float average = 0;
        String messageReceived = "";
        String time = "";
        long start, end;
        
        while(System.currentTimeMillis() - begin < 300000){
        	
        	start = System.currentTimeMillis();
        	
        	try{
                modem.write(echo_request_code.getBytes());
                String message = "";
                
                for(;;){
                    k = modem.read();
                    if(k == -1) break;
	                
                    message += (char)k;
	                
	                if(message.contains("PSTOP")) {
	                	packages++;
	                	String[] modMessage = message.split("\\s");
	                	end = System.currentTimeMillis();
	                	average += (float) (end - start) ;
	                	time += Long.toString(end-start) + "\r\n";
	                	modMessage[3] = String.valueOf(packages);
	                	message = "";
	                	
	                	for(int i = 0 ; i < modMessage.length ; i++) {
	                		message += modMessage[i];
	                		message += " ";
	                	}
	                	break; 	                	
	                }
                }

                message += "\r\n";
                //System.out.print(message);
                messageReceived +=message;
                
                
                
            } catch(Exception x) {
            	
            }
        	
        }
        
        average /= packages;
        time += "Time average is " + Float.toString(average) + " ms \r\n";
        System.out.println("Number of packages: " + packages);
        System.out.println("Ending at: " + LocalDateTime.now());
        
        try {
        	echo_stream.write(messageReceived.getBytes());
        	time_stream.write(time.getBytes());
        	echo_stream.close();
        	time_stream.close();
        } catch(Exception x) {
        	
        }
        
    }
    
    /*
     * Create a new jpeg image.
     * Receive a number of pixels until the delimiter is found from Ithaki.
     * Bytes of an image of an Egnatia street camera in is sent and we add it to a ArrayList until we find the end delimiter.
     * After the image receiving is completed we enter the data of the ArrayList into a byte[] array then we stream it to the jpeg file and a picture is created.
     */
    
    public static void imageErrorFree(Modem modem) throws FileNotFoundException{
        byte[]  imgBytes = image_request_code.getBytes();
        modem.write(imgBytes);
        
        File picture = new File("Session1/pic1.jpeg");
        FileOutputStream pictureStream = new FileOutputStream(picture);
        
        int currentPixel;
        int lastPixel = 0;
        boolean receiveImage = false;
        
        ArrayList<Byte> img =  new ArrayList<Byte>();
        //Search for image's delimiters
        while(!receiveImage){
            currentPixel = modem.read();
            if(currentPixel == 255){
                lastPixel = currentPixel;
                currentPixel = modem.read();
                if(currentPixel == 216){
                	receiveImage = true;
                    System.out.println("Image Receiving Started. ");
                    img.add((byte) lastPixel);
                    img.add((byte) currentPixel);
                }
            }
        }

        if(!receiveImage) {
            System.err.println("No delimeter - Exiting app");
            System.exit(0);
        }
        //Start receiving the image's pixels from Ithaki until the exit delimiter is found and store every byte into an Arraylist 
        for(;;){
            currentPixel = modem.read();
            if (currentPixel == -1) break;
            if(lastPixel == 255 && currentPixel == 217){
                System.out.println("Image Receiving Ended");
                img.add((byte)currentPixel);
                break;
            }
            img.add((byte)currentPixel);
            lastPixel = currentPixel;
        }

        System.out.println("Completed at: " + LocalDateTime.now());
        //Convert the ArrayList<Byte> to a byte array (byte[])
        byte[] convertedBytes = new byte[img.size()];
        Iterator<Byte> iterator = img.iterator();
        
        for(int i = 0 ; i < convertedBytes.length ; i++) convertedBytes[i] = iterator.next().byteValue();
        //Write the bytes to the picture file
        try{
            pictureStream.write(convertedBytes);
            pictureStream.close();
        } catch(Exception x){
            System.out.println(x);
        }

    }
/*
 * Same function with imageErrorFree() function with a twist: Ithaki after the halfway point of the image transmission starts adding pseudo-noise
 * thus the bottom half of the image starts to get warped.
 */
    public static void imageWithError(Modem modem) throws FileNotFoundException{
        byte[]  imgBytes = image_request_codeWithError.getBytes();
        modem.write(imgBytes);
        
        File pictureWithError = new File("Session1/pic1error.jpeg");
        FileOutputStream pictureStream = new FileOutputStream(pictureWithError);
        
        int currentPixel;
        int lastPixel = 0;
        boolean imageReceived = false;
        
        ArrayList<Byte> img =  new ArrayList<Byte>();

        while(!imageReceived){
            currentPixel = modem.read();
            if(currentPixel == 255){
                lastPixel = currentPixel;
                currentPixel = modem.read();
                if(currentPixel == 216){
                    imageReceived = true;
                    System.out.println("Image Receiving Started. ");
                    img.add((byte) lastPixel);
                    img.add((byte) currentPixel);
                }
            }
        }

        if(!imageReceived) {
            System.err.println("No delimeter - Exiting app");
            System.exit(0);
        }

        for(;;){
            currentPixel = modem.read();
            if (currentPixel == -1) break;
            if(lastPixel == 255 && currentPixel == 217){
                System.out.println("Image Receiving Ended");
                img.add((byte)currentPixel);
                break;
            }
            img.add((byte)currentPixel);
            lastPixel = currentPixel;
        }

        System.out.println("Completed at: " + LocalDateTime.now());
        byte[] convertedBytes = new byte[img.size()];
        Iterator<Byte> iterator = img.iterator();
        
        for(int i = 0 ; i < convertedBytes.length ; i++) convertedBytes[i] = iterator.next().byteValue();
        
        try{
            pictureStream.write(convertedBytes);
            pictureStream.close();
        } catch(Exception x){
            System.out.println(x);
        }
        
    }


	public static void GPS_cords_image(Modem modem) throws FileNotFoundException{
		byte[] gps_first_time = ("P6234" + "R=1010070\r").getBytes();
		
		int k;
		
		
        //for first time only we need GPS info: We use echo packages receiving technique to get data so we can create the image
        
		long begin = System.currentTimeMillis();
      
     	String messageReceived = "";
     	modem.write(gps_first_time);
      
     	while(System.currentTimeMillis() - begin < 300000){
	
	      	try{
	              modem.write(gps_first_time);
	              String message = "";
	              
	              for(;;){
	                  k = modem.read();
	                  if(k == -1) break;
		                
	                  message += (char)k;
		                
		                if(message.contains("STOP ITHAKI GPS TRACKING\r\n")) {
		                	
		                	break; 	                	
		                }
	              }
	
	              message += "\r\n";
	              messageReceived = message;
	              break;
	              
	      	} catch(Exception x) {}    	
	      }
      	//System.out.println(messageReceived);
      	
     	byte[] gpscode = ("P6234" + GPS_code(messageReceived) + "\r").getBytes();
     	//We create the image from GPS - we use the technique from the image functions
		modem.write(gpscode);
	
        File GPSpicture = new File("Session1/pic1GPS.jpeg");
        FileOutputStream pictureStream = new FileOutputStream(GPSpicture);
        
        int currentPixel;
        int lastPixel = 0;
        boolean receiveImage = false;
        
        ArrayList<Byte> img =  new ArrayList<Byte>();
        //Search for image's delimiters
        while(!receiveImage){
            currentPixel = modem.read();
            if(currentPixel == 255){
                lastPixel = currentPixel;
                currentPixel = modem.read();
                if(currentPixel == 216){
                	receiveImage = true;
                    System.out.println("Image Receiving Started. ");
                    img.add((byte) lastPixel);
                    img.add((byte) currentPixel);
                }
            }
        }

        if(!receiveImage) {
            System.err.println("No delimeter - Exiting app");
            System.exit(0);
        }
        //Start receiving the image's pixels from Ithaki until the exit delimiter is found and store every byte into an Arraylist 
        for(;;){
            currentPixel = modem.read();
            if (currentPixel == -1) break;
            if(lastPixel == 255 && currentPixel == 217){
                System.out.println("Image Receiving Ended");
                img.add((byte)currentPixel);
                break;
            }
            img.add((byte)currentPixel);
            lastPixel = currentPixel;
        }

        System.out.println("Completed at: " + LocalDateTime.now());
        
        //Convert the ArrayList<Byte> to a byte array (byte[]) 
        
        byte[] convertedBytes = new byte[img.size()];
        Iterator<Byte> iterator = img.iterator();
        for(int i = 0 ; i < convertedBytes.length ; i++) 
        	convertedBytes[i] = iterator.next().byteValue();
        
        //Write the bytes to the picture file
        
        try{
            pictureStream.write(convertedBytes);
            pictureStream.close();
        } catch(Exception x){
            System.out.println(x);
        }
		
         
	}
	
	//Takes input the message from the GPS (10 SECONDS TIME DIFFERENCE) and makes a sequence of T= parameters
	
	public static String GPS_code(String msg) {
		String R1 = "1003070\r";
		int times = Integer.parseInt(R1.substring(5, 7)) / 10;
		String[] str_breakdown = msg.split("\r\n");
		String[] samples = new String[times];
		int ctr = 0;
		for(int i = 0 ; i<str_breakdown.length - 1; i++) {
			str_breakdown[i] += "\r\n";
			if( (i - 1) % 10 == 0  ) {
				samples[ctr] = str_breakdown[i];
				ctr++;
			}
			
		}
		
		String FINAL = "T=";
		for(int i = 0 ; i < ctr ; i++) {
			if(i != ctr - 1) {
				FINAL += "2257" + String.valueOf((int) (Integer.parseInt(samples[i].substring(36, 40))*0.006)) + 
						"4037" + String.valueOf((int) (Integer.parseInt(samples[i].substring(23, 27))*0.006)) + "T=";
			}
			else FINAL += "2257" + String.valueOf((int) (Integer.parseInt(samples[i].substring(36, 40))*0.006)) + 
					"4037" + String.valueOf((int) (Integer.parseInt(samples[i].substring(23, 27))*0.006));
		}
		return FINAL;
	}
	
	public static void ARQ_handling(Modem modem) throws FileNotFoundException {
		byte[] ackreq = ACK_request_code.getBytes();

		File Output_from_arq = new File("Session1/Arq.txt");
		FileOutputStream arq_stream = new FileOutputStream(Output_from_arq);

		File Time_for_arq = new File("Session1/Time_arq.txt");
		FileOutputStream time_arq_stream = new FileOutputStream(Time_for_arq);

		long start, end;
		int k, ACK_pack = 0, all_pack = 0;
		int retry = 0;
		int[] NACK_pack_retry = new int[9];
		String text = "", all_text = "", response = "";
		long new_begin = System.currentTimeMillis();
		
		while (System.currentTimeMillis() - new_begin < 300000) {
			all_text = all_text + text + "\r\n"; 
			modem.write(ackreq);
			start = System.currentTimeMillis();
			text = "";
			
			for (;;) {
				k = modem.read();
				if (k != -1) {
					text += (char) k;
					if (text.indexOf("PSTOP") > -1) {
						if (ARQ_check(text)) {
							ACK_pack += 1;
							end = System.currentTimeMillis();
							response += Long.toString(end - start) + "\r\n";
						} else {
							retry = 0;
							while (!ARQ_check(text)) {
								text = "";
								retry++;
								modem.write(NACK_request_code.getBytes());
								for (;;) {
									k = modem.read();
									if (k != -1) {
										text += (char) k;
										if (text.indexOf("PSTOP") > -1) {
											break;
										}
									} else
										break;

								}

							}
							end = System.currentTimeMillis();
							response += Long.toString(end - start) + "\r\n";
							NACK_pack_retry[retry-1]++;
							break;

						}
						all_pack += 1;
						break;
					}
				} else
					break;

			}
			if (k == -1 && text == "")
				break;

		}
		int NACK_pack = 0;
		for (int i : NACK_pack_retry) {
			NACK_pack += i;
		}
		all_pack = ACK_pack + NACK_pack;
		System.out.println("All packages " + all_pack);
		System.out.println("ACK packages " + ACK_pack);
		System.out.println("NACK packages " + NACK_pack);
		for (int i = 0; i < NACK_pack_retry.length; i++) {
			int index = i + 1;
			System.out.println(index + " retries " + " time " + NACK_pack_retry[i]);
		}
		System.out.println("ARQ Error at " + LocalDateTime.now());
		try {
			arq_stream.write(all_text.getBytes());
			arq_stream.close();
			time_arq_stream.write(response.getBytes());
			time_arq_stream.close();
		} catch (Exception x) {
			System.out.println(x);
		}


	}
		
	
	
	public static boolean ARQ_check(String sample) {
		String text;
		int FCS;
		text = sample.substring(sample.indexOf("<") + 1, sample.indexOf(">"));
		
		int curr;
		int last = (int) text.charAt(0);
		for(int i = 1; i < text.length(); i++) {
			curr = (int) text.charAt(i);
			last = (last ^curr);
		}
		FCS = Integer.parseInt(sample.substring(sample.indexOf(">") +2, sample.indexOf("PSTOP")-1));
		return FCS == last;
	}
}
